import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncentivosPage } from './incentivos.page';

describe('IncentivosPage', () => {
  let component: IncentivosPage;
  let fixture: ComponentFixture<IncentivosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncentivosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncentivosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
