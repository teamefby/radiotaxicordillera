import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';


@Component({
  selector: 'app-regiatro-vale',
  templateUrl: './regiatro-vale.page.html',
  styleUrls: ['./regiatro-vale.page.scss'],
})
export class RegiatroValePage implements OnInit {

  constructor(private camera: Camera,private base64ToGallery: Base64ToGallery) { }
  image:string=null;
  ngOnInit() {
  }
  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
      this.base64ToGallery.base64ToGallery(imageData).then(
        res => console.log('Saved image to gallery ', res),
        err => console.log('Error saving image to gallery ', err)
      );
    })
    .catch(error =>{
      console.error( error );
    });
  }
}
