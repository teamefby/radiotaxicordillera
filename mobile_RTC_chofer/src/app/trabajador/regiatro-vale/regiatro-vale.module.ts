import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegiatroValePage } from './regiatro-vale.page';

const routes: Routes = [
  {
    path: '',
    component: RegiatroValePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegiatroValePage]
})
export class RegiatroValePageModule {}
