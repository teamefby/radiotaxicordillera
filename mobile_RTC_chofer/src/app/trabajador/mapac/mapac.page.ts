import { Component, OnInit, NgZone } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  Environment,
  GoogleMapOptions,
  BaseArrayClass,
  Polyline,
  Spherical
} from '@ionic-native/google-maps/ngx';
import {
  ToastController,
  Platform,
  LoadingController,
  AlertController,

} from '@ionic/angular';
import { ILatLng } from '@ionic-native/google-maps';
declare var google;
@Component({
  selector: 'app-mapac',
  templateUrl: './mapac.page.html',
  styleUrls: ['./mapac.page.scss'],
})
export class MapacPage implements OnInit {
 map:GoogleMap;
  loading: any;
 distance: string;
  mapRef = null;
  //otro

  constructor(   
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private geolocation: Geolocation,
    private platform: Platform,
    private _ngZone: NgZone
    )
  {

   }

   ngOnInit() {
    //this.presentAlertMultipleButtons();
    //await this.platform.ready();
    this.geom();
    //this.loadMap();
  }
  //geolocalizacion
  geom(){
    this.geolocation.getCurrentPosition().then((geo) => {
                
      //console.log(geo);
     this.loadMap({lat:geo.coords.latitude,lng:geo.coords.longitude});
     
     this.getPosition();
     }).catch((error) => {
       console.log('Error getting location', error);
     });
  
    }
    /*
   loadMap(geo:any) {
    const mapEle: HTMLElement = document.getElementById('map');
    this.mapRef = new GoogleMap(mapEle, {
      camera:{
        target: geo,
        zoom: 18,
        tilt: 30
      },
      

    });
    
  

  }*/
  getPosition(): void{
    this.mapRef.getMyLocation()
    .then(response => {
      this.mapRef.moveCamera({
        target: response.latLng
      });
      this.mapRef.addMarker({
        title: 'Mi Posicion',
        icon: 'blue',
        animation: 'DROP',
        position: response.latLng
      });
    })
    .catch(error =>{
      console.log(error);
    });
  }
  
//array para linea de trazado
 async loadMap(geo:any) {

    let points: Array<ILatLng> = [
      geo,
      {lat: 37.421980, lng: -122.083980}
    ];
    this.map = GoogleMaps.create('map_canvas', {
      camera: {
        target: points
      }
    });
    let polyline: Polyline = this.map.addPolylineSync({
      points: points
    });
    let baseArray: BaseArrayClass<ILatLng> = polyline.getPoints();

    baseArray.mapAsync((point: ILatLng, next: (newElement: any) => void) => {
      this.map.addMarker({
        position: point,
        draggable: true
      }).then(next);
    }).then((markers: Marker[]) => {

      let baseArray2: BaseArrayClass<Marker> = new BaseArrayClass<Marker>(markers);
      baseArray2.forEach((marker: Marker, idx: number) => {
        marker.on('position_changed').subscribe((params) => {
          baseArray.setAt(idx, params[1]);
        });
      });

      // trigger the position_changed event for the first calculation.
      markers[0].trigger('position_changed', null, markers[0].getPosition());
    });

    baseArray.on('set_at').subscribe(() => {
      this._ngZone.run(() => {
        let distanceMeter: number = Spherical.computeLength(baseArray);
        this.distance = (distanceMeter * 0.000621371192).toFixed(2) + " miles";
      });
    });

  }

  /*
    loadMap(geo:any) {
      this.map = GoogleMaps.create('map_canvas', {
        camera: {
          target: geo ,
          zoom: 18,
          tilt: 30
        }
      });
      
    }

  
  
  getPosition(): void{
    this.map.getMyLocation()
    .then(response => {
      this.map.moveCamera({
        target: response.latLng
      });
      this.map.addMarker({
        title: 'Mi Posicion',
        icon: 'blue',
        animation: 'DROP',
        position: response.latLng
      });
    })
    .catch(error =>{
      console.log(error);
    });
  }/*
  async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      header: 'Viaje',
      subHeader: 'Subtitle',
      message: 'Tines una Vieja',
      buttons: ['Cancel', 'Aceptar']
    });

    await alert.present();
  }*/



}
