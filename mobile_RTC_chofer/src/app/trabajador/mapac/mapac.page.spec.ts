import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapacPage } from './mapac.page';

describe('MapacPage', () => {
  let component: MapacPage;
  let fixture: ComponentFixture<MapacPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapacPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapacPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
