import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteValePage } from './reporte-vale.page';

describe('ReporteValePage', () => {
  let component: ReporteValePage;
  let fixture: ComponentFixture<ReporteValePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteValePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteValePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
