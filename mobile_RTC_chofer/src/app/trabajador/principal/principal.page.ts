import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AppComponent } from 'src/app/app.component';
import { ModalController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';


@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {
  
  encodedData='';
  constructor(private auth:AuthenticationService, private modalController:ModalController, private barcodeScanner:BarcodeScanner, private fingerprintAIO:FingerprintAIO) {  }
  
  ngOnInit() { }

  iniciarJornada(tipoInicio:number ){
    let rutTrabajador='rutTrabajador';
    switch (tipoInicio) {
      case 1:
        this.createQr(rutTrabajador);
        break;
      case 2:
        this.createFingerPrint();
        break;
      case 3:
        this.createLlamada();
        break;
      default:
        break;
    }
  }



  createQr(rutTrabajador:string){
    let cadenaValidacion =rutTrabajador + "&" + new Date().getMilliseconds().toString();
      this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE,cadenaValidacion).then((encodedData) => {
          this.encodedData = encodedData;
      }, (err) => {
          console.log("Error occured : " + err);
      });                 
  }

  createFingerPrint(){
    this.fingerprintAIO.show({
      clientId: 'Fingerprint-Demo',
      clientSecret: 'password', //Only necessary for Android
      disableBackup:true,  //Only for Android(optional)
      localizedFallbackTitle: 'Use Pin', //Only for iOS
      localizedReason: 'Please authenticate' //Only for iOS
  })
  .then((result: any) => console.log(result))
  .catch((error: any) => console.log(error));
  }

  createLlamada(){
    //aqui va el metodo que iniciara la llamada telefonica
  }

  terminarJornada():boolean{
    return true;
  }
}
