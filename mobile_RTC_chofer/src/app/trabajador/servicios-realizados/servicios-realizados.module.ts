import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ServiciosRealizadosPage } from './servicios-realizados.page';

const routes: Routes = [
  {
    path: '',
    component: ServiciosRealizadosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ServiciosRealizadosPage]
})
export class ServiciosRealizadosPageModule {}
