import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcepcionesPage } from './excepciones.page';

describe('ExcepcionesPage', () => {
  let component: ExcepcionesPage;
  let fixture: ComponentFixture<ExcepcionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcepcionesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcepcionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
