import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CentroCostoPage } from './centro-costo.page';

describe('CentroCostoPage', () => {
  let component: CentroCostoPage;
  let fixture: ComponentFixture<CentroCostoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CentroCostoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CentroCostoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
