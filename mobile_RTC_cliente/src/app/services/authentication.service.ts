import { Injectable } from '@angular/core';
import { resolve } from 'url';
import { reject } from 'q';
import { BehaviorSubject } from 'rxjs';

export interface User{
  name : string;
  role : number;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private Empleador = {name:"empleador",pass:"123456",role:0};
  private Trabajador = {name:"t",pass:"1",role:1};

  currentUser : User;
  authenticationState = new BehaviorSubject(false);
  Pages=[];

  constructor() { }

  login(userName:string,passWord:string) : Promise<boolean>{
    return new Promise((resolve,reject) => {
      if(userName===this.Empleador.name && passWord===this.Empleador.pass){
        this.currentUser={
          name:this.Empleador.name,
          role:this.Empleador.role
        }
        this.Pages=[
          {title:"Principal",url:"/principalEmpleador",icon:"planet"},
          {title:"Centro de Costos",url:"/centroCosto",icon:"planet"},
          {title:"Listado de Trabajadores",url:"/trabajadores",icon:"planet"},
          {title:"Cálculo Vacaciones Días",url:"/vacacionesDia",icon:"planet"},
          {title:"Cálculo Vacaciones Pesos",url:"/vacacionesPesos",icon:"planet"},
          {title:"Provisión Indemnización",url:"/indemnizacion",icon:"planet"},
          {title:"Reporte Asistencia Mensual",url:"/reporteAsistencia",icon:"planet"},
          {title:"Reporte Excepciones",url:"/excepciones",icon:"planet"},
          {title:"Incentivos",url:"/incentivos",icon:"planet"},
          {title:"Carta Amonestación",url:"/amonestacion",icon:"planet"},
          {title:"Registro Justificación",url:"/justificacion",icon:"planet"}
        ];
        this.authenticationState.next(true);
        resolve(true);
      }else if (userName===this.Trabajador.name && passWord===this.Trabajador.pass) {
        this.currentUser={
          name:this.Trabajador.name,
          role:this.Trabajador.role
        }
        this.Pages=[
          {title:"Principal",url:"/principalTrabajador",icon:"planet"},
          {title:"Horario Trabajador",url:"/horario",icon:"planet"},
          {title:"Reporte Asistencia",url:"/asistencia",icon:"planet"},
          {title:"Historico Asistencias",url:"/historicoAsistencia",icon:"planet"},
          {title:"Ayuda",url:"/ayuda",icon:"planet"},
          {title:"cmapa",url:"/cmapa",icon:"planet"},
          {title:"viajes",url:"/viajes",icon:"planet"}
        ];
        this.authenticationState.next(true);
        resolve(true);
      }else{
        this.authenticationState.next(false);
        reject(false);
      }
    });
  }

  isLoggedIn(){
    return this.authenticationState.value;
  }

  logout(){
    this.authenticationState.next(true);
    this.currentUser = null;
  }

  isEmpleador(){
    return this.currentUser.role === this.Empleador.role;
  }
  
  getMenuItems(){
    return this.Pages;
  }
}
