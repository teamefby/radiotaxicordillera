import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AlertController, MenuController,PopoverController } from '@ionic/angular';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import {
  ToastController,
  Platform,
  LoadingController
} from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  GeocoderResult,
  Geocoder,
  MyLocationOptions,
  LocationService
} from '@ionic-native/google-maps';
import { AgendaPage } from '../agenda/agenda.page';

@Component({
  selector: 'app-cmapa',
  templateUrl: './cmapa.page.html',
  styleUrls: ['./cmapa.page.scss'],
})
export class CmapaPage implements OnInit {
  map: GoogleMap;
  loading: any;
  m:any;
x:"hola";
  @ViewChild('search_address') search_address: ElementRef;
  constructor(
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private platform: Platform,
    private popoverController:PopoverController
  ) { }

  async ngOnInit() {
    await this.platform.ready();
    await this.loadMap();
    this.presentAlertMultipleButtons();
  }

async abrirmodal(ev:Event){
  const popover=await this.popoverController.create({
    component:AgendaPage,
    componentProps: {
      'prop1': this.x
    }
  });
  popover.present();
}
  loadMap() {
    let option:MyLocationOptions={
      enableHighAccuracy:true
    }
    
    LocationService.getMyLocation(option).then((m:MyLocation)=>{
      
      console.log(this.search_address);
      (this.search_address as any).value = 'chile';
      this.map = GoogleMaps.create('map_canvas',{
        camera:{
          target:m.latLng,
          zoom:30
        
        }
        
      });
      
     
    let marker:Marker=this.map.addMarkerSync({
        position:m.latLng,
        title:"Estas aqui"

    });
    
    marker.showInfoWindow();
    })


  }
  async onboton2_click(){
  
      const alertController = document.querySelector('ion-alert-controller');
      await alertController.componentOnReady();
    
      const alert = await alertController.create({
        header: 'Alert',
        subHeader: 'Subtitle',
        message: 'This is an alert message.',
        buttons: ['OK']
      });
      return await alert.present();
    
  }
  async onButton1_click(event) {
    this.loading = await this.loadingCtrl.create({
      message: 'Espere por favor..'
    });
    await this.loading.present();
    //this.map.clear();
    
    // Address -> latitude,longitude
    
    Geocoder.geocode({
      "address": (this.search_address as any).value
    })
    .then((results: GeocoderResult[]) => {
      //console.log(results);
      this.loading.dismiss();

      if (results.length > 0) {
        let marker: Marker = this.map.addMarkerSync({
          'position': results[0].position,
          'title':  JSON.stringify(results[0].position)
        });

        this.map.animateCamera({
          'target': marker.getPosition(),
          'zoom': 17
        });
        marker.showInfoWindow();
      } else {
        alert("No Encontrado");
      }
    });
  }
    async presentAlertMultipleButtons() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['Cancel', 'Open Modal', 'Delete']
    });

    await alert.present();
  }
  async onButton2_click(event) {
    this.loading = await this.loadingCtrl.create({
      message: 'Espere por favor..'
    });
    await this.loading.present();
    //this.map.clear();
    
    // Address -> latitude,longitude
  let option:MyLocationOptions={
    enableHighAccuracy:true
  } ; 
  this.loading.dismiss();
  LocationService.getMyLocation(option).then((location: MyLocation)=>{
    this.map.animateCamera({
    target:location,
    zoom:17
    });
    let marker: Marker = this.map.addMarkerSync({
      'position': location.latLng,
      'title': 'mi pos'
    });
    this.map.animateCamera({
      'target': location.latLng,
      'zoom': 17
    });
    marker.showInfoWindow();
  })
  
      //console.log(results);

  }
}
