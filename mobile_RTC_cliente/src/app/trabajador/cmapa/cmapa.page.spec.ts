import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmapaPage } from './cmapa.page';

describe('CmapaPage', () => {
  let component: CmapaPage;
  let fixture: ComponentFixture<CmapaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmapaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmapaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
