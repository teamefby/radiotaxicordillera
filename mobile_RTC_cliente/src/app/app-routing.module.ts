import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'principalTrabajador', loadChildren: './trabajador/principal/principal.module#PrincipalPageModule' },
  { path: 'horario', loadChildren: './trabajador/horario/horario.module#HorarioPageModule' },
  { path: 'ayuda', loadChildren: './trabajador/ayuda/ayuda.module#AyudaPageModule' },
  { path: 'asistencia', loadChildren: './trabajador/asistencia/asistencia.module#AsistenciaPageModule' },
  { path: 'historicoAsistencia', loadChildren: './trabajador/historico-asistencia/historico-asistencia.module#HistoricoAsistenciaPageModule' },
  { path: 'principalEmpleador', loadChildren: './empleador/principal/principal.module#PrincipalPageModule' },
  { path: 'centroCosto', loadChildren: './empleador/centro-costo/centro-costo.module#CentroCostoPageModule' },
  { path: 'trabajadores', loadChildren: './empleador/trabajadores/trabajadores.module#TrabajadoresPageModule' },
  { path: 'vacacionesDia', loadChildren: './empleador/vacaciones-dia/vacaciones-dia.module#VacacionesDiaPageModule' },
  { path: 'vacacionesPesos', loadChildren: './empleador/vacaciones-pesos/vacaciones-pesos.module#VacacionesPesosPageModule' },
  { path: 'indemnizacion', loadChildren: './empleador/indemnizacion/indemnizacion.module#IndemnizacionPageModule' },
  { path: 'reporteAsistencia', loadChildren: './empleador/reporte-asistencia/reporte-asistencia.module#ReporteAsistenciaPageModule' },
  { path: 'excepciones', loadChildren: './empleador/excepciones/excepciones.module#ExcepcionesPageModule' },
  { path: 'incentivos', loadChildren: './empleador/incentivos/incentivos.module#IncentivosPageModule' },
  { path: 'amonestacion', loadChildren: './empleador/amonestacion/amonestacion.module#AmonestacionPageModule' },
  { path: 'justificacion', loadChildren: './empleador/justificacion/justificacion.module#JustificacionPageModule' },
  { path: 'cmapa', loadChildren: './trabajador/cmapa/cmapa.module#CmapaPageModule' },
  { path: 'viajes', loadChildren: './trabajador/viajes/viajes.module#ViajesPageModule' },
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
