import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimientobancarioComponent } from './movimientobancario.component';

describe('MovimientobancarioComponent', () => {
  let component: MovimientobancarioComponent;
  let fixture: ComponentFixture<MovimientobancarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimientobancarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimientobancarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
