import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrovaleComponent } from './registrovale.component';

describe('RegistrovaleComponent', () => {
  let component: RegistrovaleComponent;
  let fixture: ComponentFixture<RegistrovaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrovaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrovaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
