import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards/auth.guard';
import { AgendarserComponent } from './agendarser/agendarser.component';
import { ServiciosactualesComponent } from './serviciosactuales/serviciosactuales.component';
import { ControlfacturaComponent } from './controlfactura/controlfactura.component';
import { GenerarfacturaComponent } from './generarfactura/generarfactura.component';
import { RegistrovaleComponent } from './registrovale/registrovale.component';
import { MovimientobancarioComponent } from './movimientobancario/movimientobancario.component';
import { ReportesComponent } from './reportes/reportes.component';
import { RclientesComponent } from './rclientes/rclientes.component';
import { RempresaComponent } from './rempresa/rempresa.component';
import { RcolaboradoresComponent } from './rcolaboradores/rcolaboradores.component';

const routes: Routes = [
  {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
  {
    path: 'rcolaboradores',
    component: RcolaboradoresComponent,
    canActivate: [AuthGuard]
},
{
  path: 'rempresa',
  component: RempresaComponent,
  canActivate: [AuthGuard]
},
{
  path: 'rclientes',
  component: RclientesComponent,
  canActivate: [AuthGuard]
},
{
  path: 'Reportes',
  component: ReportesComponent,
  canActivate: [AuthGuard]
},
{
  path: 'movimientobancario',
  component: MovimientobancarioComponent,
  canActivate: [AuthGuard]
},
  {
    path: 'registrovale',
    component: RegistrovaleComponent,
    canActivate: [AuthGuard]
},
  {
    path: 'controlfactura',
    component: ControlfacturaComponent,
    canActivate: [AuthGuard]
},
  {
    path: 'generarfactura',
    component: GenerarfacturaComponent,
    canActivate: [AuthGuard]
},
  {
    path: 'agendarser',
    component: AgendarserComponent,
    canActivate: [AuthGuard]
},
{
  path: 'serviciosactuales',
  component: ServiciosactualesComponent,
  canActivate: [AuthGuard]
},
    {
        path: 'login',
        component: LoginComponent
    },
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
