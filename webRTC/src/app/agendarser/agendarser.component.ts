import { Component, OnInit, ViewChild,TemplateRef, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
//plugin full calendario v4 dejo comentario con la pagina
//https://fullcalendar.io/docs/v4#toc
import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import  interactionPlugin   from '@fullcalendar/interaction';
import * as bootstrap from 'bootstrap';//aqui se tuvo que tomar verciones de type scrip al descargar verciones no son las mismas de la documentacion dejo comentario con la pagina
//https://stackoverflow.com/questions/32735396/error-ts2339-property-modal-does-not-exist-on-type-jquery
import 'fullcalendar';
import { Title } from '@angular/platform-browser';
import eventSources from '@fullcalendar/core/reducers/eventSources';

@Component({
  selector: 'app-agendarser',
  templateUrl: './agendarser.component.html',
  styleUrls: ['./agendarser.component.css'],


})

export class AgendarserComponent implements OnInit {
  constructor(private modal: NgbModal) { }

  ngOnInit() {
   
    document.getElementById("menuGroup_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("menuGroup_mn").classList.add("active");
    document.getElementById("Home").classList.remove("active");
    document.getElementById("Agendar Servicio").classList.add("active");
    document.getElementById("Servicios Actuales").classList.remove("active");
    document.getElementById("Generar Factura").classList.remove("active");
    document.getElementById("Control Factura").classList.remove("active");
    document.getElementById("Registro Vale").classList.remove("active");
    document.getElementById("Movimiento Bancario").classList.remove("active");
    document.getElementById("Reportes").classList.remove("active");
    document.getElementById("Registro Cliente").classList.remove("active");
    document.getElementById("Registro Empresa").classList.remove("active");
    document.getElementById("Registro Colaboradores").classList.remove("active");
 

    this.calendario();//se llama a la funcion para que se inicialize
  }
  //se crea funcion calendario para ordenar
  calendario(){
    
    //variables de datos para agendar
    let toArray=[];
    let Arrayed=[];
    var inputValue;
    var hora1;
    var ch;
    var dire;
    var id=0;
    var ch1;
    var idc;
    var arrch=[];
    var color = '#';
//funcion para el calendario
     $(function() {
 
      var calendarEl = document.getElementById('calendar');

      var calendar = new Calendar(calendarEl, {
        //plugins de uso 
        plugins: [ dayGridPlugin,timeGridPlugin ,interactionPlugin ],
        dragScroll: true,
        defaultView: 'timeGridDay',
        locale:'es',
        //encabezado del calendario
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
          
        },
        //texto de los botones del calendario
        buttonText: {
          today:    'Hoy',
          month:    'Mes',
          week:     'Semana',
          day:      'Dia',
          
        },
//funcion click al calendario
        dateClick: function(info) {
          $("#myModal").modal();//abrir modal
          id+=1;
         var f=new Date();//toma hora y fecha actual
         var c=f.toString();
        var s = new String(info.date.toISOString()); // esto toma la fecha y lo almacena en una variable
        toArray = s.split("T");//lo separa y almacena en un array
        var cadena2 = c.slice(16, -38);
          (<HTMLInputElement>document.getElementById("hora")).value=cadena2;//pasa el valor de la variale hora al input

        },
        //eventos click funcion para hacer cosas al cliquear ub¿n evento    arrch
        eventClick: function(info) {
          var s = new String(info.event.id);
          idc=s.toString();
         // alert(idc);
          var event =calendar.getEventById(idc);
          var datos = event.title // a property (a Date object)
          var start = event.start // a property (a Date object)
          Arrayed = datos.split("||");//lo separa y almacena en un array
          var c=start.toString();
          var cadena2 = c.slice(16, -38);
          arrch = Arrayed[1].split(":");
          $("#miModal").modal();//abrir modal
          (<HTMLInputElement>document.getElementById("edprueba")).value=Arrayed[0];//pasa el valor de la variale hora al input
          (<HTMLInputElement>document.getElementById("edchofer")).value=arrch[1];//pasa el valor de la variale hora al input
          (<HTMLInputElement>document.getElementById("eddireccion")).value=Arrayed[2];//pasa el valor de la variale hora al input
          (<HTMLInputElement>document.getElementById("edhora")).value=cadena2;//pasa el valor de la variale hora al input
          var someEvents = calendar.getEvents().filter(function(event) {
            return event.title === 'My Event';
          });
        },
        //aqui se pueden pooner evendos por defecto
        events: [

        ]
        
      });
      $("#myBtn").click(function(){
        var myArray = [
          'red',
          'pink',
          'green',
          'orange',
          'brown',
          'crimson',
          'golden',
          'purple',
          'blue'
      ];
      var rand = myArray[Math.floor(Math.random() * myArray.length)];
        
        console.log(rand);
        //aqui se tomal los value de las cajas de texto y se pasan a variable de esta forma se hace aqui en angular se diferencia de js normal dejo comentario con la pagina
        //https://stackoverflow.com/questions/12989741/the-property-value-does-not-exist-on-value-of-type-htmlelement
        inputValue = (<HTMLInputElement>document.getElementById("prueba")).value; //variable nombre cliente
        ch = (<HTMLInputElement>document.getElementById("chofer")).value; //variable nombre chfer
        hora1 = (<HTMLInputElement>document.getElementById("hora")).value;//variable hora 
        dire = (<HTMLInputElement>document.getElementById("direccion")).value;//variable direccion
        //con esta funcion se agregar eventos
        calendar.addEvent({
          id:id,
          title:  'cliente: '+inputValue+' || Chofer: '+ch+' || Dirección: '+dire ,//titulo del evento
          start:toArray[0]+'T'+hora1,//inicio del evento 
          color:rand,//color de la etiqute  del evento
          textColor:"white",//color texto
       
        });
        
      });

      //boton para editar nombre chofer
      $("#miBtn").click(function(){
        ch1 = (<HTMLInputElement>document.getElementById("edchofer")).value; //variable nombre chfer
        var event = calendar.getEventById(idc);
          event.setProp('title','cliente: '+inputValue+' || Chofer: '+ch1+' || Dirección: '+dire ,);
          //event.remove();
        
      });
      //boton para editar nombre chofer
      $("#miBt").click(function(){
        var event = calendar.getEventById(idc);
        event.remove();
      });
      calendar.render();//renderisa el evento
    });
    
  
    }
    guardar(forma){
      console.log("valor", forma.value);

    }
    guardared(forma){
      console.log("valor", forma.value);

    }
}



