import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendarserComponent } from './agendarser.component';

describe('AgendarserComponent', () => {
  let component: AgendarserComponent;
  let fixture: ComponentFixture<AgendarserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendarserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendarserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
