import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlfacturaComponent } from './controlfactura.component';

describe('ControlfacturaComponent', () => {
  let component: ControlfacturaComponent;
  let fixture: ComponentFixture<ControlfacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlfacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlfacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
