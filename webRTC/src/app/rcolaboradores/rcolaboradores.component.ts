import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms' 
@Component({
  selector: 'app-rcolaboradores',
  templateUrl: './rcolaboradores.component.html',
  styleUrls: ['./rcolaboradores.component.css']
})
export class RcolaboradoresComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    document.getElementById("menuGroup_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("menuGroup_mn").classList.add("active");
    document.getElementById("Home").classList.remove("active");
    document.getElementById("Agendar Servicio").classList.remove("active");
    document.getElementById("Servicios Actuales").classList.remove("active");
    document.getElementById("Generar Factura").classList.remove("active");
    document.getElementById("Control Factura").classList.remove("active");
    document.getElementById("Registro Vale").classList.remove("active");
    document.getElementById("Movimiento Bancario").classList.remove("active");
    document.getElementById("Reportes").classList.remove("active");
    document.getElementById("Registro Cliente").classList.remove("active");
    document.getElementById("Registro Empresa").classList.remove("active");
    document.getElementById("Registro Colaboradores").classList.add("active");
  }
  guardar(forma:NgForm){
    console.log("Formulario Guardado");
  
    console.log("valor", forma.value);
  
  }
}
