import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RcolaboradoresComponent } from './rcolaboradores.component';

describe('RcolaboradoresComponent', () => {
  let component: RcolaboradoresComponent;
  let fixture: ComponentFixture<RcolaboradoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RcolaboradoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RcolaboradoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
