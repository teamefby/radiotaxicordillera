import { Component, OnInit } from '@angular/core';
import { User } from '../_models/user';
import { UserService } from '../_services/user.service';
import { first } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  users:User[];
  
  titulo="Home";

  constructor(private userService:UserService) { }

  ngOnInit() {
    document.getElementById("menuGroup_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("menuGroup_mn").classList.add("active");
    document.getElementById("Home").classList.add("active");
    document.getElementById("Agendar Servicio").classList.remove("active");
    document.getElementById("Servicios Actuales").classList.remove("active");
    document.getElementById("Generar Factura").classList.remove("active");
    document.getElementById("Control Factura").classList.remove("active");
    document.getElementById("Registro Vale").classList.remove("active");
    document.getElementById("Movimiento Bancario").classList.remove("active");
    document.getElementById("Reportes").classList.remove("active");
    document.getElementById("Registro Cliente").classList.remove("active");
    document.getElementById("Registro Empresa").classList.remove("active");
    document.getElementById("Registro Colaboradores").classList.remove("active");
  }

}
