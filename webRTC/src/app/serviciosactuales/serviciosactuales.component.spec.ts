import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiciosactualesComponent } from './serviciosactuales.component';

describe('ServiciosactualesComponent', () => {
  let component: ServiciosactualesComponent;
  let fixture: ComponentFixture<ServiciosactualesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiciosactualesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiciosactualesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
