import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-serviciosactuales',
  templateUrl: './serviciosactuales.component.html',
  styleUrls: ['./serviciosactuales.component.css']
})
export class ServiciosactualesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    document.getElementById("menuGroup_lnk").setAttribute("aria-expanded", "true")
    document.getElementById("menuGroup_mn").classList.add("active");
    document.getElementById("Home").classList.remove("active");
    document.getElementById("Agendar Servicio").classList.remove("active");
    document.getElementById("Servicios Actuales").classList.add("active");
    document.getElementById("Generar Factura").classList.remove("active");
    document.getElementById("Control Factura").classList.remove("active");
    document.getElementById("Registro Vale").classList.remove("active");
    document.getElementById("Movimiento Bancario").classList.remove("active");
    document.getElementById("Reportes").classList.remove("active");
    document.getElementById("Registro Cliente").classList.remove("active");
    document.getElementById("Registro Empresa").classList.remove("active");
    document.getElementById("Registro Colaboradores").classList.remove("active");
  }

}
