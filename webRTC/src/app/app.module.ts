import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { fakeBackendProvider } from './_helpers/fake-backend';
import { Interceptor } from './_helpers/interceptor';
import { ErrorInterceptor } from './_helpers/error-interceptor';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AgendarserComponent } from './agendarser/agendarser.component';
import { ServiciosactualesComponent } from './serviciosactuales/serviciosactuales.component';
import { GenerarfacturaComponent } from './generarfactura/generarfactura.component';
import { ControlfacturaComponent } from './controlfactura/controlfactura.component';
import { RegistrovaleComponent } from './registrovale/registrovale.component';
import { MovimientobancarioComponent } from './movimientobancario/movimientobancario.component';
import { ReportesComponent } from './reportes/reportes.component';
import { RclientesComponent } from './rclientes/rclientes.component';
import { RempresaComponent } from './rempresa/rempresa.component';
import { RcolaboradoresComponent } from './rcolaboradores/rcolaboradores.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AgendarserComponent,
    ServiciosactualesComponent,
    GenerarfacturaComponent,
    ControlfacturaComponent,
    RegistrovaleComponent,
    MovimientobancarioComponent,
    ReportesComponent,
    RclientesComponent,
    RempresaComponent,
    RcolaboradoresComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModalModule,
    FormsModule
    
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
