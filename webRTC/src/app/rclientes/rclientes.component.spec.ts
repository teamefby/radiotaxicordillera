import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RclientesComponent } from './rclientes.component';

describe('RclientesComponent', () => {
  let component: RclientesComponent;
  let fixture: ComponentFixture<RclientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RclientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RclientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
