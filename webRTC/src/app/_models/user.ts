export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    imgPersonal:string;
    token?: string;
    menu:{
        nombreMenu:string, 
        groupIcon:string, 
        pages:{url:string,name:string,icon:string}[]
    }[];
}